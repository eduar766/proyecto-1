var main = function() {
	$('.icon-menu').click( function() {   //Seleccionamos el elemento .icon-menu para añadirle una operacion al momento de hacer click sobre el.
		$('.menu').animate({
			left:'0px'
		}, 200);  //Añadimos el efecto de animacion que consiste en desplazar a la izquierda el menu, para dejar ver las opciones.

		$('body').animate({  //Añadimos esta animacion, para que el cuerpo del html tambien se desplaze hacia la izquierda.
			left: '285px'
		}, 200);

	}); 

	$('.icon-menu').click(function{  //añadimos esta otra funcion que debe ejecutar el boton de menu al volver a hacer click sobre el
		$('.menu').animate({
			left:"-285px"
		}, 200); //regresamos el menu a su posicion original

		$('body').animate({
			left:"0px" //regresamos body a su posicion original que debe ser 0px.
		}, 200);
	});

	};

$(document).ready(main); //codigo jquery que espera a que cargue el documento entero para ejecutar la funcion main.
